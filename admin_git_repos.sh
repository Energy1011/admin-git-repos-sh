#!/bin/bash

# 	El script sirve para realizar tareas sencillas de git
#   Copyright (C) <2014>  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com)

#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.

#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

# El script sirve para realizar tareas sencillas de git (para personas que lo comienzan a utilizar y no están muy familiarizadas con los comandos) el script puede apuntar a un servidor de repos git.
# Con este escript podemos inicializar un repo, agregar remote, subir repo al servidor por ssh, clonar repositorio en el servidor para ambiente de pruebas,
# realizar actualización fetch de un repo en el servidor, etc..

ipServer="localhost";
tipoConexion="ssh://";
sshUser="git";
pathServerRepos="/var/gitroot";
pathServerTest="/var/www/html";

function show_menu(){
	echo "**************************************************";
	echo "1-Inicializar repositorio GIT local.";
	echo "2-Subir repo local al servidor y agregar remote. ($pathServerRepos)";
	echo "3-Clonar repo desde el servidor ($pathServerRepos) a MI EQUIPO";
	echo "4-Clonar repo COMPLETO a (PRUEBAS $pathServerTest)";
	echo "5-Actualizar repo en pruebas (Pull [fetch & merge] en repo de $pathServerTest)";
	echo "6-Forzar fetch completo a mi local (traer todos los cambios del server con hard reset [ dejar todos los archivos locales igual al server, perdiendo los cambios locales] )";
	echo "7-Listar los repos existentes en el servidor $ipServer";
	echo "8-Listar los repos existentes en el servidor con ultimo commit $ipServer";
	echo "9-Hacer fetch a todos los repos locales (git fetch origin master)";
	echo "0-Salir";
	echo "*************************************************";
	read option;

	case $option in
		1 ) #Inicializar repo local
			init_repo $1
			;;

		2 ) #Crear repo en el servidor
			create_remote_empty_repo $1
			;;

		3 ) #Clonar repo a mi local
			git_clone $1
			;;

		4 ) #Clonando repositorio completo a ambiente de pruebas
			clone_remote_repo_to_htdocs $1
			;;

		5 ) #Actualizando repo en ambiente pruebas
			update_repo $1
			;;

		6 ) #Fetch del servidor a mi local borrando todos los cambios
			fetch_all_and_hard_reset $1
			;;

		7 ) #Listar los repos
			list_repos
			;;

		8 ) #Listar los repos con su ultimo commit realizado
			list_repos_with_last_commit
			;;

		9 ) #Fetch all repos en local
			fetch_all_local_repos
			;;

		0 ) echo "Terminando script..."
			exit;
			;;
	esac
}

function fetch_all_local_repos(){
	 find * -maxdepth 0 -type d -exec sh -c '( cd {} && echo "-----------------git fetch origin master-------------------------" && echo "Fecth a: {} (CTRL+D tres veces para cancelar este fetch)" && git fetch origin master && echo " ")' ';'
	 show_menu;
}

function list_repos_with_last_commit(){
 	stringCommand="cd $pathServerRepos && find *.git -maxdepth 0 -type d -exec sh -c '( cd {} && echo \"\n------------------------Repo------------------------\" && pwd && git log -1)' ';' && echo \" \" && echo \"********* DATOS GENERALES **********\" && echo \"Cantidad de repos:\" && ls | grep .git | wc -l && echo \" \" && echo \"IP:$ipServer\"";
	ssh "$sshUser"@"$ipServer" "$stringCommand";
	show_menu;
}


function list_repos(){
 	stringCommand="cd $pathServerRepos && find *.git -maxdepth 0 -type d -exec sh -c '(cd {} && echo \"\nRepo:\" && pwd)' ';' && echo \" \" && echo \"********* DATOS GENERALES **********\" && echo \"Cantidad de repos:\" && ls | grep .git | wc -l && echo \" \" && echo \"IP:$ipServer\" ";
	ssh "$sshUser"@"$ipServer" "$stringCommand";
	show_menu;
}

function  fetch_all_and_hard_reset(){
	if [ -z $1 ]; then
		echo "Ingrese el nombre del repo o proyecto sin .git";
		read repo_name;
	else
		repo_name=$1;
	fi
	echo "mireponame";
	echo $repo_name;

	if ! folder_exist $repo_name; then
		cd $repo_name;
		echo "Este comando puede reemplazar archivos locales con modificaciones recientes en el proyecto (se recomienda respaldar), ya que el proyecto local quedará igual al del servidor";
		echo "¿Desea continuar? (y/n):";
		read confirm;
		if [ "$confirm" == "y" ]; then
			git fetch --all
			git reset --hard origin/master
			echo "Completado... Ahora su repositorio se encuentra igual al origin/master.";
		else
			echo "Script terminado...";
		fi
	fi
}

function folder_exist(){
	   	if [ -d "$repo_name" ]; then
	   		#la carpeta existe
			return 1;
		fi
		return 0;
}

function git_clone(){
	if [ -z $1 ]; then
		echo "Ingrese el nombre del repo o proyecto sin .git";
		read repo_name;
	else
		repo_name=$1;
	fi
		if ! folder_exist $repo_name; then
			echo "La carpeta '$repo_name' ya existe en el directorio actual:";
			pwd
			echo "Si necesita clonar nuevamente el repositorio: mueva, renombre o borre la carpeta.";
			exit;
			else
			echo "Clonando repo $pathServerRepos/$repo_name.git";
			git clone "$sshUser"@"$ipServer":$pathServerRepos/"$repo_name.git";
			if [ $? -ne 0 ]; then
				echo "Verifique que el repositorio $pathServerRepos/$repo_name.git exista en el servidor.";
				exit;
			fi
			echo "Repositorio clonado correctamente.";
		fi
}

function verify_parameter(){
	if [ -z $repo_name ]; then
		echo "Por favor pase como <parametro> el nombre de la carpeta donde se encuentra el proyecto.";
		echo "ejemplo: sh admin_git_repos.sh <nombre-carpeta-proyecto>";
		exit;
	fi
}

function cd_to_repo_folder(){
	if [ ! -d $repo_name ]; then
		echo "Nombre de proyecto incorrecto, no existe carpeta con ese nombre.";
		echo "Ejecute el script en el mismo nivel de directorio donde se encuentre la carpeta del proyecto.";
		echo "Ejemplo: admin_git_repos.sh <nombre-carpeta-proyecto>";
		exit;
	fi
	cd "$repo_name";
}

function git_init(){
	# Se inicializa el repositorio git
	git init
	if [ $? -ne 0 ]; then
		echo "Git init: Error al inicializar el repositorio. ¿ Git se encuentra instalado en el sistema ?";
		exit
	else
		cd ..
	fi
}

function create_empty_repo(){
	# Se crea un repositorio vacio con --bare
	git clone --bare "$repo_name" "$repo_name.git";
	if [ $? -ne 0 ]; then
		echo "Git clone --bare: No se ha podido crear el repositorio vacio local para subirlo al servidor.";
		exit
	fi
}

function upload_repo_files(){
	# Comprobar si existe un repo vacio local, si existe lo borramos para evitar el error:
	#fatal: destination path ... already exists and is not an empty directory.
	if [ -d "$repo_name.git" ]; then
		echo "Existe un repo vacio local $repo_name.git y se volvera a generar...";
		rm -rf "$repo_name.git";
	fi
	create_empty_repo $repo_name
	# Se suben los archivos al sevidor con scp
	echo "Subiendo archivos al servidor...";
	scp -r "$repo_name.git" "$sshUser"@"$ipServer":$pathServerRepos
	if [ $? -ne 0 ]; then
		echo "scp: Ha fallado el intento de subir los archivos al servidor.";
		exit
	else
		echo "Archivos cargados correctamente.";
	fi
}

function remove_empty_repo_folder(){
	# Elimino la carpeta temporal creada por git clone despues de subirla
	echo "Limpiando carpeta local temporal de repo '$repo_name.git'"
	rm -rf "$repo_name.git";
}

function add_remote(){
	# Agrego en remote para el repositorio
	echo "Agregando remote origin...";
	cd "$1/";
	echo "Agregando remote al repositorio...";
	git remote add origin "$sshUser"@"$ipServer":$pathServerRepos/"$repo_name.git";
}

function create_remote_empty_repo(){
		if [ -z $1 ]; then
		echo "Ingrese el nombre del repo o proyecto sin .git";
		read repo_name;
	else
		repo_name=$1;
	fi
	echo "Creando repo en el servidor...";
	upload_repo_files $repo_name
	remove_empty_repo_folder $repo_name
	add_remote $repo_name
}

function init_repo(){
	if [ -z $1 ]; then
		echo "Ingrese el nombre del repo o proyecto sin .git";
		read repo_name;
	else
		repo_name=$1;
	fi
	echo "Inicializando repositorio de carpeta '$1'...";
	cd_to_repo_folder $repo_name
	git_init $repo_name
	echo "Repo local inicializado :)";
	echo "Ahora es buen momento para agregar archivos al stage y hacer commit de archivos.";
	echo "Cuando ya se han realizado los commit locales de los archivos de proyecto:";
	echo "Es necesario realizar el paso 2 de este script para que se agregue el remote y posteriormente se puedan hacer los push.";
}

function clone_remote_repo_to_htdocs(){
	if [ -z $1 ]; then
		echo "Ingrese el nombre del repo o proyecto sin .git";
		read repo_name;
	else
		repo_name=$1;
	fi
	echo "Clonando repo dentro de $pathServerTest...";
	stringCommand="cd $pathServerTest; git clone $pathServerRepos/$repo_name.git;";
	ssh "$sshUser"@"$ipServer" $stringCommand
}

function update_repo(){
	if [ -z $1 ]; then
		echo "Ingrese el nombre del repo o proyecto sin .git";
		read repo_name;
	else
		repo_name=$1;
	fi
	stringCommand="cd $pathServerTest/$repo_name; git pull origin master;";
	ssh "$sshUser"@"$ipServer" $stringCommand
}


function main(){
	show_menu $1
}

main $1